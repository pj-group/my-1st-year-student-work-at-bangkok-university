import sqlite3
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import time as tm
from note import showProgress


def mainwindow() :
    global menubar,emptyMenu

    root = Tk()
    #w = 600 #width of application
    #h = 800 #height of application
    x = root.winfo_screenwidth()/2 - w/2
    y = root.winfo_screenheight()/2 - h/2
    root.geometry("%dx%d+%d+%d"%(w,h,x,y))

    root.rowconfigure((0,1,2),weight=1)
    root.columnconfigure((0,1,2),weight=1)

    emptyMenu = Menu(root) #create empty menu
    root.config(bg='#0a0a0a',menu=emptyMenu) #change root menu to  empty menu 
    #root.attributes('-alpha',0.95)  # Make Transparency low
    root.option_add('*font',"Calibri 24 bold")
    #root.title("Login/Register Application: ")
    
    menubar = Menu(root)
    menubar.add_command(label="Logout",command=logoutClick)
    #menubar.add_command(label="Exit",command=root.quit)
    return root


def loginlayout() :
    global ent_username
    global ent_password
    global loginframe
    global btn_exit
    global mo_loginframe
    global cbt_rememberme

    root.title("Login")
    root.config(menu=emptyMenu)

    loginframe = Label(root,image=img_hotel_bg,bg='#0a0a0a')
    loginframe.grid(row=0,column=0,columnspan=4,rowspan=3,sticky='news')

    loginframe.rowconfigure((0,1,2),weight=1)
    loginframe.rowconfigure((3,4,5,6),weight=1)
    loginframe.columnconfigure((0,1,2),weight=1)

    Label(loginframe,text="SIGN IN FORM",font="Calibri 26 bold",compound=LEFT,bg='#fcfbf9',fg='black').grid(row=1,pady=10,columnspan=3,sticky=N)

    ent_username = Entry(loginframe,width=30,bg='#191919',fg='light grey',textvariable=userinfo,justify=CENTER,highlightthickness=0)#
    ent_username.grid(row=1,column=1,ipady=12,sticky=S)
    
    ent_password = Entry(loginframe,text='Password',bg='#191919',fg='light grey',width=30,show='*',textvariable=pwdinfo,justify=CENTER,highlightthickness=0)
    ent_password.grid(row=2,column=1,ipady=12,sticky=N)
    
    #cbt_rememberme = Checkbutton(loginframe,text='   Remember me',bg='#8a8c73',font="Calibri 14 bold",fg='black',variable=cbtinfo)
    #cbt_rememberme.place(x=360,y=440)

    Button(loginframe,text="SIGN IN",font='Bold 16',width=28,command=loginclick).grid(row=3,column=1,sticky=S)
    #btn_regis = Button(loginframe,text="Register now!",font='Bold 16',width=28).grid(row=4,column=1,sticky=S)
    btn_exit = Button(loginframe,text="Exit",font='Bold 16',width=28,command=exitClick).grid(row=4,column=1,sticky=S)


def checkin_window(std_id):
    global newmain
    global checkinframe
    global btn_profile
    global left
    global in_rightframe

    loginframe.destroy()
    
    w = 1600 #width of application
    h = 800 #height of application
    x = root.winfo_screenwidth()/80 - w/80
    y = root.winfo_screenheight()/80 - h/80
    root.geometry("%dx%d+%d+%d"%(w,h,x,y+60))

    root.rowconfigure((0,1,2),weight=1)
    root.columnconfigure((0,1,2),weight=1)

    root.title("Welcome : "+ str(std_id))
    emptyMenu = Menu(root)
    root.config(menu=emptyMenu) #Add menubar to newwindow menu
    root.attributes("-fullscreen", False)
  
    checkinframe = Frame(root,bg='black')
    checkinframe.grid(row=0,column=0,rowspan=3,columnspan=3,stick='news')
    
    checkinframe.rowconfigure((0,1,2),weight=1)
    checkinframe.columnconfigure(0,weight=1)
    checkinframe.columnconfigure((1,2),weight=2)

    left = Frame(checkinframe,bg='orange')
    left.grid(row=0,column=0,rowspan=3,sticky='news')
    left.rowconfigure((0,1),weight=2)
    left.rowconfigure(2,weight=1)
    left.columnconfigure((0,2),weight=1)
    left.columnconfigure(1,weight=2)

    right = Frame(checkinframe,bg='black')
    right.grid(row=0,column=1,rowspan=3,columnspan=3,sticky='news')
    right.rowconfigure((0,2),weight=1)
    right.rowconfigure(1,weight=4)
    right.columnconfigure((0,1,2),weight=1)
    # ต้องใช้ values

    in_rightframe = Frame(right,bg='orange')
    in_rightframe.grid(row=1,column=1,stick='news')

    in_rightframe.rowconfigure((0,1,2,3,4,5),weight=1)
    in_rightframe.columnconfigure((0,1,2),weight=1)

    Label(in_rightframe,text='ข้อตกลง\nพนักงาน/ผู้ดูแลระบบ',bg='orange',font="Bold 24").grid(row=0,column=1)

    value_rbt = 1
    mode = ['']
    for i,des in enumerate(des_list) :
        Label(in_rightframe,text=des_list[i],font="Bold 16",bg='orange',justify=LEFT).grid(row=1+i,column=1,padx=20)  # Text(ข้าพเจ้ายอมรับผิด....)
        Checkbutton(in_rightframe,bg='orange',variable=spy1[i],onvalue=1,offvalue=0,command=click_cbt_check).grid(row=1+i,column=1,sticky=W)
    
    sql_employee = "SELECT * FROM student WHERE std_id=?"
    cursor.execute(sql_employee,[std_id])
    result_employee = cursor.fetchone()

    print(result)
    print(result_employee)
    print(result_employee[0])
    print(result_employee[1])
    
    btn_profile = Button(left,image=img_num1,bg='black',command=Click_in_widget) # ปุ่มรูป profile
    btn_profile.grid(row=0,column=1,pady=50)

    '''
    # สั่งเปิด Profile_window
    if result == "1630708351, 'Mr.1', 'bruh', 45, '1', '1'" :
        btn_profile = Button(left,image=img_num1,bg='black',command=Click_in_widget)
        btn_profile.grid(row=0,column=1,pady=50)

    elif result == result_employee[1] :
        btn_profile = Button(left,image=img2,bg='black',command=Click_in_widget)
        btn_profile.grid(row=0,column=1,pady=50)
    else :
        print("Error อีกล้า")
    '''

def mainprogram_window() :
    global mainframe
    global clock_label
    global left, right, in_right
    global btn_home, btn_checkin, btn_exit
    
    

    root.attributes("-fullscreen", True)
    root.config(menu=menubar)

    mainframe = Frame(root)
    mainframe.grid(row=0,column=0,rowspan=3,columnspan=3,stick='news')
    
    mainframe.rowconfigure(0,weight=1)
    mainframe.rowconfigure((1,2,3),weight=2)
    mainframe.rowconfigure((4,5),weight=1)
    mainframe.columnconfigure(0,weight=1)
    mainframe.columnconfigure((1,2),weight=1)

    top = Label(mainframe,bg='black') # เอารูปมาใส่
    top.grid(row=0,column=0,columnspan=3,sticky='news')
    top.rowconfigure(0,weight=1)
    top.rowconfigure(1,weight=1)
    #top.columnconfigure(2,weight=0)
    top.columnconfigure((0,1,2),weight=1)

    in_top = Frame(top,bg='black')
    in_top.grid(row=1,column=0,rowspan=1,columnspan=3,sticky='news')
    in_top.columnconfigure((0,1,2,3,4,5,6,7,8,9,10),weight=1)
    in_top.rowconfigure(0,weight=1)

    left = Frame(mainframe,bg='orange')
    left.grid(row=1,column=0,rowspan=6,sticky='news')

    right = Frame(mainframe,bg='sky blue')
    right.grid(row=1,column=0,rowspan=6,columnspan=3,sticky='news')
    right.rowconfigure((0,1,2),weight=1)

    btn_home = Button(in_top,text="Home",fg='white',bg='black',command=click_home,relief=SOLID)
    btn_home.grid(row=0,column=0,rowspan=1,padx=10)

    btn_checkin = Button(in_top,text="Check-In",fg='white',bg='black',command=checkin,relief=SOLID)
    btn_checkin.grid(row=0,column=1,rowspan=1,padx=10)

    btn_exit = Button(in_top,text="Exit",fg='white',bg='black',command=root.quit,relief=SOLID)
    btn_exit.grid(row=0,column=2,rowspan=1,padx=10)

    clock_label = Label(top,font='ariel 20',bg='black',fg='red') # ปรับขนาดนาฬิกาตรง font
    clock_label.grid(row=0,column=2,sticky=NE)

    Label(right,image=img_travel_bg).pack(fill=BOTH)

    btn_home.bind("<Enter>", on_enter_home)
    btn_home.bind("<Leave>", on_leave_home)

    btn_checkin.bind("<Enter>", on_enter_checkin)
    btn_checkin.bind("<Leave>", on_leave_checkin)

    btn_exit.bind("<Enter>", on_enter_exit)
    btn_exit.bind("<Leave>", on_leave_exit)


def checkin() :
    createTreeview()
    

def createTreeview() : # create Treeview and Entry(in checkin)
    global conn,cursor,mytree,i
    global right, in_right_bot
    global lb_totalprice
    global ent_search, ent_refnumber, ent_building, ent_room, ent_roomtype, ent_price, ent_status

    left.destroy()

    right = LabelFrame(mainframe,text='Checkin page',font='Bold 20',bg='sky blue') # Light green
    right.grid(row=1,column=0,rowspan=3,columnspan=3,sticky='news')
    right.columnconfigure(0,weight=1)
    right.columnconfigure((1,2,3,4),weight=1)

    in_right_bot = Frame(mainframe,bg='purple') # Purple
    in_right_bot.grid(row=4,column=0,rowspan=2,columnspan=3,sticky='news')
    in_right_bot.rowconfigure(0,weight=1)

    #Create TreeView Frame
    treeframe = Frame(right)
    treeframe.pack(padx=20,pady=20,fill=X)
    #Create Scrollbar
    treebar = Scrollbar(treeframe)
    treebar.pack(side=RIGHT,fill=Y)
    #Create Treeview
    mytree = ttk.Treeview(treeframe,height=10,columns=["Customer_Ref","Building","Room","Roomtype","Day","Price","Status"],yscrollcommand=treebar.set)
    mytree.pack(side=BOTTOM,fill=X)
    #config scrollbar on the treeview
    treebar.config(command=mytree.yview) # เสาะหาความรู้อันแสนปวดร้าว

    #create headings
    mytree.heading("#0",text='')
    mytree.heading("Customer_Ref",text="Ref_number",anchor=CENTER)
    mytree.heading("Building",text="Building",anchor=CENTER)
    mytree.heading("Room",text="Room",anchor=CENTER)
    mytree.heading("Roomtype",text="Roomtype",anchor=CENTER)
    mytree.heading("Day",text="Day",anchor=CENTER)
    mytree.heading("Price",text="Price",anchor=CENTER)
    mytree.heading("Status",text="Status",anchor=CENTER)
    #Format our columns
    mytree.column("#0",width=0,stretch=NO)
    mytree.column("Customer_Ref",width=20,anchor=CENTER)
    mytree.column("Building",width=20,anchor=CENTER)
    mytree.column("Room",width=20,anchor=CENTER)
    mytree.column("Roomtype",width=20,anchor=CENTER)
    mytree.column("Day",width=20,anchor=CENTER)
    mytree.column("Price",width=20,anchor=CENTER)
    mytree.column("Status",width=20,anchor=CENTER)

    mytree.delete(*mytree.get_children()) #delete oldte data from treeview
    
    btn_search = Button(right,image=img_search,command=fetch_search)
    btn_search.place(x=550,y=300)

    ent_search = Entry(right,width=30,textvariable=search_info)
    ent_search.place(x=20,y=300)

    #Label(right,text="Ref_customer",font='Bold 18',bg='light green').pack(fill=X)
    ent_refnumber = Entry(right,textvariable=refnumber_info,font='Bold 16',fg='blue',justify=CENTER,state=DISABLED) # DISABLED state
    ent_refnumber.pack(padx=10,side=LEFT)

    #Label(right,text="Building",font='Bold 18',bg='light green').pack(fill=X)
    ent_building = Entry(right,textvariable=building_info,font='Bold 16',fg='blue',justify=CENTER)
    ent_building.pack(padx=10,side=LEFT)

    #Label(right,text="Room",font='Bold 18',bg='light green').pack(fill=X)
    ent_room = Entry(right,textvariable=room_info,font='Bold 16',fg='blue',justify=CENTER)
    ent_room.pack(padx=10,side=LEFT)

    #Label(right,text="Roomtype",font='Bold 18',bg='light green').pack(fill=X)
    ent_roomtype = Entry(right,textvariable=roomtype_info,font='Bold 16',fg='blue',justify=CENTER)
    ent_roomtype.pack(padx=10,side=LEFT)

    ent_status = Entry(right,textvariable=status_info,font='Bold 16',fg='blue',justify=CENTER)
    ent_status.pack(padx=10,side=LEFT)

    Label(right,text="Day : ",font='Bold 16',bg='sky blue').place(x=390,y=650)
    Spinbox(right,width=3,from_=0,to=30,font='Bold 18',textvariable=spbcount_info,command=calculate_totalprice,justify=CENTER).place(x=450,y=650)
    Label(right,text="Price : ",font='Bold 16',bg='sky blue').place(x=610,y=650)
    ent_price = Entry(right,width=20,font='Bold 24',fg='blue',textvariable=price_info,justify=CENTER)
    ent_price.place(x=700,y=645)

    btn_addrecord = Button(right,text="add record",command=insert_task)
    btn_addrecord.pack(side=BOTTOM,padx=10,expand=TRUE)

    btn_delete = Button(right,text="Delete",command=delete_task)
    btn_delete.pack(side=BOTTOM,padx=10,expand=TRUE)

    btn_availiability = Button(right,text="Availiability",command=update_task_ava)
    btn_availiability.pack(side=BOTTOM,padx=10,expand=TRUE)

    btn_Unavailiability = Button(right,text="Unavailiability",command=update_task_unava)
    btn_Unavailiability.pack(side=BOTTOM,padx=10,expand=TRUE)

    lb_totalprice = Label(in_right_bot,text="*** Show Total Price ***",fg='black',font='Bold 48')
    lb_totalprice.pack(expand=TRUE,fill=BOTH)

    mytree.bind('<Double-1>',treeviewclick) # click ใน Treeview
  
    get_checkin() # ใส่ข้อมูลใน data ลง Treeview




#fetch search data
def fetch_search():
    mytree.delete(*mytree.get_children())
    sql = "select * from checkin where Room=?"
    cursor.execute(sql,[search_info.get()])
    result = cursor.fetchall()
    if result:
        for i,data in enumerate(result):
            if data[6] == 0 :
                data6 = "Availability"
            
            elif data[6] == 1 :
                data6 = "Unavailability"

            data6_insearch = str(data[6])
            data6_insearch = 'ไม่ว่างปะวะ'
            mytree.insert('','end',values=(data[0],data[1],data[2],data[3],data[4],data[5],data6))



def get_checkin() : # ใส่ข้อมูลลง Treeview
    global result_checkin
    global data1, data2, data3, data4, data5, data6
    global i
    
    mytree.delete(*mytree.get_children()) #delete oldte data from treeview
    conn = sqlite3.connect('db/PROJECT_CS311.db')
    cursor = conn.cursor()
    sql = "SELECT * FROM checkin"
    cursor.execute(sql)# ORDER BY firstname ASC")
    result_checkin = cursor.fetchall() 

    mytree.tag_configure('oddrow',background='white')
    mytree.tag_configure('evenrow',background='lightgrey')
    
    for i,data in enumerate(result_checkin):
        data0 = str(data[0])+'#'
        data1 = str(data[1])
        data2 = str(data[2])
        data3 = str(data[3])
        data4 = str(data[4])
        data5 = str(data[5])
        data6 = str(data[6])

        if data[6] == 0 :
            data6 = "Availability"
            
        elif data[6] == 1 :
            data6 = "Unavailability"

        else :
            print("เอาอีกล้า")
        mytree.insert('', 'end', values=(data0,data1,data2,data3,data4,data5,data6),tags=('oddrow'))

'''
        elif data[6] == 0 :
            if i%2 == 1 :
                data6 = "Availability"
                mytree.insert('', 'end', values=(data0,data1,data2,data3,data4,data5,data6),tags=('oddrow'))
            else :
                mytree.insert('', 'end', values=(data0,data1,data2,data3,data4,data5,data6),tags=('oddrow'))
'''          

def delete_task() :
    selected = mytree.item(mytree.focus(), 'values')

    old_selected = selected[0]
    print(old_selected)
    A1 = old_selected.replace("#","")

    sql =  "DELETE FROM checkin WHERE Customer_Ref=?"
    cursor.execute(sql,[A1])
    conn.commit()
    fetch_tree()


def click_home() : # Button(Home)
    #global right
    if btn_checkin == 0 :
        right.destroy()
        in_right_bot.destroy()

    clearentry_checkin()
    mainprogram_window()

def clearentry_checkin() :
    ent_search.delete(0,END)
    ent_refnumber.delete(0,END)
    ent_building.delete(0,END)
    ent_room.delete(0,END)
    ent_roomtype.delete(0,END)
    ent_status.delete(0,END)
    ent_price.delete(0,END)
    spbcount_info.set(1)

def fetch_tree() :
    mytree.delete(*mytree.get_children())
    sql = "select * from checkin"
    cursor.execute(sql)
    result = cursor.fetchall()
    if result :
        for i,data in enumerate(result) :
            data0= str(data[0])+'#'
            
            if data[6] == 1 :
                data6 = '#Unavailability'
                mytree.insert('','end',values=(data0,data[1],data[2],data[3],data[4],data[5],data6))
            else :
                data6 = 'Availability'
                mytree.insert('','end',values=(data0,data[1],data[2],data[3],data[4],data[5],data6))


def insert_task() : # addrecord => เอาไว้เพิ่ม ห้องโรงแรม
    global i 
    i = i + 1
    if i%2 == 0 :
        mytree.insert('',text=str(cursor.lastrowid),index='end',values=(cursor.lastrowid,building_info.get(),room_info.get(),roomtype_info.get()),tags='evenrow')
    else :
        mytree.insert('',text=str(cursor.lastrowid),index='end',values=(cursor.lastrowid,building_info.get(),room_info.get(),roomtype_info.get()),tags='oddrow')
    #insert all data item into login table of SQLite DB : write insert into command here
    sql = 'INSERT INTO checkin (Building,Room,Roomtype,Day,Price) VALUES (?,?,?,?,?) '
    cursor.execute(sql,[building_info.get(),room_info.get(),roomtype_info.get(),spbcount_info.get(),price_info.get()])

    #data[0] += '#'

    conn.commit()
    retrivedata()
    fetch_tree()

def update_task_unava() :
    selected = mytree.item(mytree.focus(), 'values')

    old_selected = selected[0]
    old_selected3 = selected[3]
    print(old_selected)
    A1 = old_selected.replace("#","")

   # mytree.item(selected,values=(order_info.get()))
    print("Update record function",building_info.get(),room_info.get(),roomtype_info.get())

    #update to the login table process : write update command here

    sql =  '''UPDATE checkin
            SET Status = 1, Day =?
            WHERE Customer_Ref=?'''
    cursor.execute(sql,[spbcount_info.get(),A1])

    # sql =  "UPDATE checkin SET Day = 1 and WHERE Customer_Ref=?"
    # cursor.execute(sql,[old_selected3])

    conn.commit()
    fetch_tree()
    okforshowbill = messagebox.showinfo("Admin","Check-in Successfully")
   # if okforshowbill :
   #     createbillwindow()

def update_task_ava() :
    selected = mytree.item(mytree.focus(), 'values')

    old_selected = selected[0]
    old_selected3 = selected[3]
    print(old_selected)
    A1 = old_selected.replace("#","")

   # mytree.item(selected,values=(order_info.get()))
    print("Update record function",building_info.get(),room_info.get(),roomtype_info.get())

    #update to the login table process : write update command here

    sql =  '''UPDATE checkin
            SET Status = 0, Day='None'
            WHERE Customer_Ref=?'''
    cursor.execute(sql,[A1])

    # sql =  "UPDATE checkin SET Day = 1 and WHERE Customer_Ref=?"
    # cursor.execute(sql,[old_selected3])

    conn.commit()
    fetch_tree()
    okforshowbill = messagebox.showinfo("Admin","Check-in Successfully")


def createbillwindow() :
    global billwindow
    global lsb_bill

    billwindow = Toplevel()
    billwindow.title("Welcome : ")
    emptyMenu = Menu(billwindow)
    billwindow.config(bg='skyblue',menu=emptyMenu) #Add menubar to newwindow menu

    w = 600
    h = 800
    x = billwindow.winfo_screenwidth()/2 - w/2
    y = billwindow.winfo_screenheight()/80 - h/80
    billwindow.geometry("%dx%d+%d+%d"%(w,h,x,y+60))
    billwindow.rowconfigure((0,1),weight=1)
    billwindow.columnconfigure((0,1),weight=1)

    left = Frame(billwindow,bg='red')
    left.grid(row=0,column=0,rowspan=2,sticky='news')
    left.rowconfigure((0,1,2),weight=1)
    left.columnconfigure((0,1,2),weight=1)

    right = Frame(billwindow,bg='pink')
    right.grid(row=0,column=1,rowspan=2,sticky='news')

    right.rowconfigure((0,1,2),weight=1)
    right.columnconfigure((0,1,2),weight=1)

    Label(left,text="Ref_number"+"\n"+"Building"+"\n"+"Room"+"\n"+"Roomtype"+"\n"+"Day"+"\n"+"Price").grid(row=1,column=1,sticky=W)
    Label(left,text=choose[0]+"\n"+choose[1]+"\n"+choose[2]+"\n"+choose[3]+"\n"+choose[4]+"\n"+choose[5]).grid(row=1,column=1,sticky=E)

    Label(right,text=total+"Bath"+"\n"+total_25).grid(row=1,column=1)
'''
    billframe = Frame(billwindow)
    billframe.pack(fill=X)

    scrollbar_listbox = Scrollbar(billframe)
    scrollbar_listbox.pack(side=RIGHT,fill=BOTH)

    lsb_bill = Listbox(billframe,activestyle='dotbox')
    lsb_bill.pack(fill=X)

    lsb_bill.config(yscrollcommand = scrollbar_listbox.set) 
    lsb_bill.config(command = lsb_bill.yview) # scrollbar ให้มามารถเลื่อน ขึ้น-ลง ได้

    lsb_bill.insert(END,choose)
'''
    


def calculate_totalprice() :
    global lb_promo_25
    global total, total_25

    count = int(spbcount_info.get())
    price = int(count) * int(choose[5])
    total = 0
    total = total + int(price)
    lb_totalprice['text'] = "%.2f"%total,"Bath"

    
    if count > 0 and count < 7 :
        lb_totalprice['text'] = "%.2f"%total,"Bath"

    elif count >= 7 and count < 15 :
        global promoframe, lb_promo_25
        promo_25 = 'โปรโมชั่น...!!!\n***เข้าพัก 7 วัน ลด(25%)***\n'
        total_25 = int(total) * 0.75
        #lb_totalprice['font'] = 'Bold 36'
        #lb_totalprice['fg'] = 'skyblue'
        

        promoframe = Frame(in_right_bot,bg='light cyan')
        promoframe.place(x=100,y=50)
        lb_promo_25 = Label(promoframe,text='โปรโมชั่น...!!!\n***เข้าพัก 7 วัน ลด(50%)***\n')
        lb_promo_25.grid(row=0,column=0)
        lb_totalprice['text'] = "%.2f"%total_25,"Bath"
    else :
        lb_totalprice['text'] = "%.2f"%total,"Bath"
        promoframe.destroy()


def treeviewclick(e) :
    global choose
    #calculate_totalprice()


    choose = mytree.item(mytree.focus(),'values')
    refnumber_info.set(choose[0])
    building_info.set(choose[1])
    room_info.set(choose[2])
    roomtype_info.set(choose[3])
    
    price_info.set(choose[5])

    if choose[6] == 'Availability' :
        ent_status['fg'] = 'green'
        
    elif choose[6] == 'Unavailability' :
        ent_status['fg'] = 'red'
    
    ent_status['font'] = 'Bold 20'
    status_info.set(choose[6])

    #lsb_showprice.insert(END, choose)
    #lsb_showprice.insert(END, total)
    
    #lb_totalprice['text'] = "%.2f"%total,"Bath"
    print(choose)


def on_enter_home(e):
    btn_home['background'] = 'sky blue'


def on_leave_home(e):
    btn_home['background'] = 'black'


def on_enter_checkin(e):
    btn_checkin['background'] = 'sky blue'


def on_leave_checkin(e):
    btn_checkin['background'] = 'black'
   
        
def on_enter_exit(e):
    btn_exit['background'] = 'dark red'


def on_leave_exit(e):
    btn_exit['background'] = 'black'

        
    
'''
    elif count < 7 : 
        #lb_totalprice['fg'] = 'black'
        #promoframe = Frame(in_right_bot,bg='light cyan')
        #promoframe.place(x=100,y=50)
        promoframe.destroy()
        #lb_totalprice['text'] = "%.2f"%total,"Bath"
    else :
        promoframe.destroy()
        
        #lb_promo_50.destroy()
'''
def display_time() :
    global current_time
    current_time = tm.strftime('%I:%M:%S:%p') 
    clock_label['text'] = current_time
    clock_label['fg'] = 'white'
    clock_label['bg'] = 'black'
    clock_label['font'] = 'Bold 22'
    clock_label.after(1,display_time)

def profilewindow(std_id) : # สร้างวินโดใหม่ชื่อ => newwindow
    global newwindow
    global profileframe, pro_id, pro_fname, pro_lname, pro_score
    global result_stu

    newwindow = Toplevel()
    newwindow.title("Welcome : "+ str(std_id))
    emptyMenu = Menu(newwindow)
    newwindow.config(bg='skyblue',menu=emptyMenu) #Add menubar to newwindow menu

    w = 600
    h = 800
    x = newwindow.winfo_screenwidth()/2 - w/2
    y = newwindow.winfo_screenheight()/80 - h/80
    newwindow.geometry("%dx%d+%d+%d"%(w,h,x,y+60))

    profileframe = Frame(newwindow, bg='skyblue')
    profileframe.columnconfigure((0,1,2),weight=1)
    profileframe.rowconfigure((0,1,2,3,4,5,6,7,8,9,10),weight=1)

    sql_student = "SELECT * FROM student WHERE std_id=?"
    cursor.execute(sql_student,[std_id])
    result_stu = cursor.fetchone()

    # รูป on the top of profile frame (ถ้า user ที่ login เป็นเพศไหน จะ create Label(image) ตาม Gender นั้นๆ )
    if result_stu[5] == 'Male' :
        Label(profileframe,image=img_male,bg=bg_color).grid(row=0,column=0,columnspan=4)
    else :
        Label(profileframe,image=img_female,bg=bg_color).grid(row=0,column=0,columnspan=4)

    Label(profileframe,text="StudentID",bg=bg_color,fg=fg_color).grid(row=1,column=0,sticky=E,padx=10)
    pro_id = Entry(profileframe,width=20,bg="#d3e0ea",textvariable=id_info,state=DISABLED)
    pro_id.grid(row=1,column=1,sticky=W,padx=10)

    Label(profileframe,text="First name",bg=bg_color,fg=fg_color).grid(row=2,column=0,sticky=E,padx=10)
    pro_fname = Entry(profileframe,width=20,bg="#d3e0ea",textvariable=fname_info,state=DISABLED)
    pro_fname.grid(row=2,column=1,sticky=W,padx=10)

    Label(profileframe,text="Last name",bg=bg_color,fg=fg_color).grid(row=3,column=0,sticky=E,padx=10)
    pro_lname = Entry(profileframe,width=20,bg="#d3e0ea",textvariable=lname_info,state=DISABLED)
    pro_lname.grid(row=3,column=1,sticky=W,padx=10)

    Label(profileframe,text="Gender",bg=bg_color,fg=fg_color).grid(row=4,column=0,sticky=E,padx=10)
    pro_gender = Entry(profileframe,width=20,bg="#d3e0ea",textvariable=gender_info,state=DISABLED)
    pro_gender.grid(row=4,column=1,sticky=W,padx=10)

    Label(profileframe,text="Address",bg=bg_color,fg=fg_color).grid(row=5,column=0,sticky=E,padx=10)
    pro_address = Entry(profileframe,width=20,bg="#d3e0ea",textvariable=address_info,state=DISABLED)
    pro_address.grid(row=5,column=1,sticky=W,padx=10)

    Label(profileframe,text="Phone",bg=bg_color,fg=fg_color).grid(row=6,column=0,sticky=E,padx=10)
    pro_phone = Entry(profileframe,width=20,bg="#d3e0ea",textvariable=phone_info,state=DISABLED)
    pro_phone.grid(row=6,column=1,sticky=W,padx=10)

    #Button(profileframe,text="Edit",command=editable).grid(row=9,column=1)
    #Button(profileframe,text="Logout",command=logoutClick).grid(row=5,column=1,pady=20)
    Button(profileframe,text="Close",command=close_profilewindow).grid(row=9,column=1,sticky=E)

    id_info.set(result_stu[0])
    fname_info.set(result_stu[1])
    lname_info.set(result_stu[2])
    gender_info.set(result_stu[5])
    address_info.set(result_stu[6])
    phone_info.set(result_stu[7])
    
    profileframe.place(x=0,y=0,width=w,height=h) 

# ล็อคอิน function
def loginclick() :
    global result
    if userinfo.get() == "":
        messagebox.showwarning("ADMIN:","You did not enter Username")
        ent_username.focus_force()
    elif pwdinfo.get() == "":
        messagebox.showwarning("ADMIN","You did not enter password")
        ent_password.focus_force()
    else:
        sql = "select * from student where username=?"
        cursor.execute(sql,[userinfo.get()])
        result = cursor.fetchall()
        if result :
            if pwdinfo.get() ==  "":
                messagebox.showwarning("ADMIN","You did not enter password")
                ent_password.focus_force()
            else :
                sql = "select * from student where username=? AND password=?"
                cursor.execute(sql,[userinfo.get(),pwdinfo.get()])
                result = cursor.fetchone()
                if result: # โปรแกรมทำงาน
                    #messagebox.showinfo("ADMIN","Login successfully")
                    #showProgress()
                    checkin_window(result[0])
                else:
                    messagebox.showwarning("ADMIN","Incorrect Username or Password")
                    ent_password.select_range(0,END)
                    ent_password.focus_force()
        else :
            messagebox.showwarning("ADMIN","Login failed\nInvaild username or password.")
            ent_username.delete(0,END)  
            ent_password.delete(0,END)   
            ent_username.select_range(0,END)


# ReCheck ข้อตกลง รอบ1 แล้ว Create CheckButton_Confirm ตั้งสติแปปนึง
def click_cbt_check() : 
    if spy1[0].get() != 1 :
        print("ยังไม่ติ๊ก cbt_check0")
    else :
        if spy1[1].get() == 0 :
            print("ยังไม่ติ๊ก cbt_check1")
        elif spy1[2].get() == 0 :
            print("ยังไม่ติ๊ก cbt_check2")
        elif spy1[3].get() == 0 :
            print("ยังไม่ติ๊ก cbt_check3")
        else :
            Label(in_rightframe,text="ข้าพเจ้ายอมรับข้อตกลงที่กล่าวข้างต้นทั้งหมด",font='Bold 14',bg='orange',fg='black').grid(row=5,column=1,padx=90,sticky=W)
            Checkbutton(in_rightframe,bg='orange',variable=cbt_info,command=click_cbt_confirm).grid(row=5,column=1,padx=40,sticky=W)


# ReCheck ข้อตกลง รอบ2 แล้ว Create Button
def click_cbt_confirm() : 
    global btn_startwork
    
    if cbt_info.get() == 1 :
        btn_startwork = Button(left,text="Start Work !",image=img2,compound = LEFT,font='Bold 24',fg='orange',bg='black',state=DISABLED,relief=RAISED,command=click_startwork) 
        btn_startwork.grid(row=1,column=1)
        btn_startwork['state'] = NORMAL
    else :
        print("อั้ยหยา")
        btn_startwork['state'] = DISABLED


def click_startwork() :
    if spy1[0].get() == 0 :
        messagebox.showwarning("Admin","จงยอมรับข้อตกลงที่ 1 เดี๋ยวนี้ !")
    elif spy1[1].get() == 0 :
        messagebox.showwarning("Admin","จงยอมรับข้อตกลงที่ 2 เดี๋ยวนี้ !")
    elif spy1[2].get() == 0 :
        messagebox.showwarning("Admin","จงยอมรับข้อตกลงที่ 3 เดี๋ยวนี้ !")
    elif spy1[3].get() == 0 :
        messagebox.showwarning("Admin","จงยอมรับข้อตกลงที่ 4 เดี๋ยวนี้ !")
    else :
        checkinframe.destroy()
        #newwindow.destroy()
        mainprogram_window()
        display_time()



def close_profilewindow():
    #checkin_window()
    #root.destroy()
    newwindow.destroy()
    #ent_username.focus()
   # ent_username.delete(0,END)
    #ent_password.delete(0,END)
    

def close_mainprogram_window():
    newmain.destroy()
'''
def editable() :
    pro_fname.config(state=NORMAL)
    pro_lname.config(state=NORMAL)
    #pro_score.config(state=NORMAL)
    #Button(profileframe,text="Update",command=update_data).grid(row=5,column=1)
'''

def update_data() :
    print('save data')
    sql = '''
            UPDATE student
            SET first_name=?, last_name=?
            WHERE std_id=?
    '''
    cursor.execute(sql,(pro_fname.get(),pro_lname.get(),pro_id.get()))
    conn.commit()
    messagebox.showinfo("Admin","Update Successfully")
    profileframe.destroy()
    mainwindow()
    ent_username.delete(0,END)
    ent_password.delete(0,END)


def enter_press(e):
    loginclick()


def exitClick() :
    #profileframe.destroy()
    #loginlayout() #Show login
    askexit = messagebox.askokcancel(title='Exit',message='Are you sure,do you want to Exit Program?')
    if askexit:
        root.quit()
        

def logoutClick() :
    askuser = messagebox.askokcancel(title='Logout',message='Are you sure,do you want to logout?')
    if askuser:
        #newmain.destroy()
        checkinframe.destroy()
        root.attributes("-fullscreen", False)
        w = 600
        h = 800
        x = root.winfo_screenwidth()/80 - w/80
        y = root.winfo_screenheight()/80 - h/80
        root.geometry("%dx%d+%d+%d"%(w,h,x,y+60))

        loginlayout()
        newwindow.destroy()    #  สั่งปิด Profile window
        clear_entry()

    else :
        print("No --> Stay boy!")


def resetlogin():
    ent_username.delete(0, END)
    ent_password.delete(0,END)
    ent_username.focus_force()   
    
def retrivedata() : # Table student from Db browser mother f..
    sql = "select * from student"
    cursor.execute(sql)
    result = cursor.fetchall()
    print("Total row = ",len(result))
    for i,data in enumerate(result) :
        print("Row#",i+1,data)

def retrivedata_checkin() : # Table checkin
    result = cursor.execute("select * from checkin")
    for data in result :
        print(data)

def Click_Entry_username(e):
    print("Click Username")


def Click_Entry_password(e):
    print("Click Password")
   

def Click_Cbt_rememberme(e):
    if cbtinfo.get() != 1 :
      print('Remember me [On]')
    else :
        print('Remember me [Off]')


def Click_in_widget() :
    profilewindow(result[0])


def createconnection() :
    global conn,cursor
    conn = sqlite3.connect('db/PROJECT_CS311.db')
    cursor = conn.cursor()    


def clear_entry(): # ใช้เคลีย Entry(username,password) ในหน้า loginlayout
    ent_username.delete(0,END)
    ent_password.delete(0,END)
    ent_username.focus()

manyrows = 10
manycols = 10

w = 600 #width of application
h = 800 #height of application

bg_color = "skyblue"
fg_color = "#f6f5f5"

createconnection()
root = mainwindow()

des_list = ['ข้าพเจ้ายินดีรับผิดทางกฏหมาย ถ้าหากมีการนำข้อมูลออกเผยแพร่สู่ระบบ',
            'ข้าพเจ้ายินดีรับผิดทางกฏหมาย ถ้าหากมีการทำข้อมูลในระบบสุญหาย     ',
            'ข้าพเจ้ายินดีรับผิดทางกฏหมาย ถ้าหากไม่ซื่อสัตย์และไม่จริงใจ               ',
            'ข้าพเจ้ายินดีรับผิดทางกฏหมาย ถ้าหากไม่ตั้งใจทำงาน                        ']

infolist2 = ['Info1','Info2','Info3','Info4']
rspy = IntVar()
rspy.set(0)

cbt_info = IntVar()
spy1 = [IntVar() for i in des_list]
#spy1.set(0)

#spylist = [spy1,spy2,spy3,spy4]


userinfo = StringVar()
pwdinfo = StringVar()
img_male = PhotoImage(file='images/male1.png').subsample(5,5)
img_female = PhotoImage(file='images/female1.png').subsample(5,5)
img2 = PhotoImage(file='images/go.png').subsample(6,6)
#img_background = PhotoImage(file='images/keyboad.png')
img_hotel_bg = PhotoImage(file='images/travel_bg.png').subsample(3,3)
img_travel_bg = PhotoImage(file='images/travel2.png').subsample(2,2)
img_num1 = PhotoImage(file='images/num1.png').subsample(2,2)
img_search = PhotoImage(file='images/search.png').subsample(6,6)

img_checkin = PhotoImage(file='images/bg_hotel_checkin_window_png.png').subsample(5,5)

id_info = StringVar() # profile
fname_info = StringVar()
lname_info = StringVar()
gender_info = StringVar()
address_info = StringVar()
phone_info = StringVar()

refnumber_info = StringVar() # checkin
building_info = StringVar()
room_info = StringVar()
roomtype_info = StringVar()
status_info = StringVar()

spbcount_info = IntVar()
spbcount_info.set(1)

cbtinfo = IntVar()

search_info = StringVar()
price_info = StringVar()

loginlayout()
ent_username.focus_force()

root.bind('<Return>', enter_press)
ent_username.bind("<Button-1>",Click_Entry_username)
ent_password.bind("<Button-1>",Click_Entry_password)
#cbt_rememberme.bind("<Button-1>",Click_Cbt_rememberme)




root.mainloop()
cursor.close() #close cursor
conn.close() #close database connection
